import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: {
      films: null,
      detailFilm: {},
      image: 'https://image.tmdb.org/t/p/w185_and_h278_bestv2/'
    }
  },
  mutations: {
    setFilm (state, payload) {
      state.data.films = payload
    },
    setDetail (state, payload) {
      state.data.detailFilm = payload
    }
  },
  actions: {
    getFilm ({ commit }) {
      axios
        .get('https://api.themoviedb.org/3/movie/now_playing?api_key=12bc0e841cf079697d78e0a8b263aae6&language=id-ID&page=1')
        .then((response) => {
          commit('setFilm', response.data.results)
          console.log(response)
        })
    },
    getDetail ({ commit }, id) {
      axios.get('https://api.themoviedb.org/3/movie/' + id + '?api_key=12bc0e841cf079697d78e0a8b263aae6')
        .then((response) => {
          commit('setDetail', response.data)
        })
    }
  }
})
